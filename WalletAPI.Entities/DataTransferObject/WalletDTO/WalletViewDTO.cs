﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WalletAPI.Entities.DataTransferObject.WalletDTO
{
    public class WalletViewDTO
    {
        public Guid WalletId { get; set; }
        public decimal Balance { get; set; }
    }
}
