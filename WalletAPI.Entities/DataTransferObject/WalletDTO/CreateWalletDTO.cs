﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WalletAPI.Entities.DataTransferObject.WalletDTO
{
    public class CreateWalletDTO
    {
        [Column(TypeName = "decimal(18,2)")]
        public decimal Opening_Balance { get; set; }
    }
}
