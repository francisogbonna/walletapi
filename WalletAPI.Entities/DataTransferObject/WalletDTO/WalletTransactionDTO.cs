﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WalletAPI.Entities.DataTransferObject.WalletDTO
{
    public class WalletTransactionDTO
    {
        public Guid WalletId { get; set; }
        public decimal  Amount { get; set; }
    }
}
