﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WalletAPI.Entities.DataTransferObject.TransactionDTO
{
    public class TransactionViewDTO
    {
        public Guid Id { get; set; }
        public Guid WalletId { get; set; }
        public string TransactionType { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public decimal Balance { get; set; }
        public string Date { get; set; } 
    }
}
