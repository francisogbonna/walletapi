﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WalletAPI.Entities.Model;

namespace WalletAPI.Entities.DataTransferObject.TransactionDTO
{
    public class CreateTransactionDTO
    {
        [Column("TransactioinId")]
        public Guid Id { get; set; }

        [ForeignKey(nameof(Wallet))]
        [Required(ErrorMessage ="Wallet ID is required")]
        public Guid WalletId { get; set; }

        [Required(ErrorMessage ="Transaction type is required")]
        public string TransactionType { get; set; }

        [Required(ErrorMessage ="short Transaction Description is required")]
        [MaxLength(80,ErrorMessage ="Description should not be more than 80 characters.")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Transaction amount is required.")]
        [Column(TypeName = "decimal(18,2)")]
        public decimal Amount { get; set; }
    }
}
