﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WalletAPI.Entities.DataTransferObject.WalletDTO;
using WalletAPI.Entities.Model;

namespace WalletAPI.Entities.DataTransferObject.UserDTO
{
    public class CreateUserDTO
    {
        [Required(ErrorMessage = "Name is required")]
        public string FullName { get; set; }

        [Required(ErrorMessage = "User must be above 18 years old.")]
        public int Age { get; set; }

        [Required(ErrorMessage ="Username is required")]
        public string UserName { get; set; }

        [Required(ErrorMessage ="Email is required")]
        public string Email { get; set; }

        [Required(ErrorMessage ="Password is required")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Address field is required.")]
        public string Address { get; set; }

        [Required(ErrorMessage = "State field is required.")]
        public string State { get; set; }

        [Required(ErrorMessage ="Residence Country is required.")]
        public string Country { get; set; }

        public CreateWalletDTO Wallet { get; set; }
    }
}
