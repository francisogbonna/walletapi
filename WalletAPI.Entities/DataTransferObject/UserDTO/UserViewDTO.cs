﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WalletAPI.Entities.DataTransferObject.UserDTO
{
    public class UserViewDTO
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }

        public int Age { get; set; }
        public string FullAddress { get; set; }
        public bool Active { get; set; }
    }
}
