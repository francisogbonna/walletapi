﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WalletAPI.Entities.Model;

namespace WalletAPI.Entities.Configuration
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            PasswordHasher<User> passwordHasher = new PasswordHasher<User>();
            var user1 = new User
            {
                Id = "336ee44d-e8ac-42ab-a102-41f4cd2f13dc",
                FullName = "Pope Francis",
                Age = 24,
                UserName = "PopeFrancis",
                NormalizedUserName = "POPEFRANCIS",
                Email = "pope@gmail.com",
                NormalizedEmail = "POPE@GMAIL.COM",
                Address = "Angwan Sanusi Kawo",
                State = "Kaduna State",
                Country = "Nigeria",
                Active = true
            };
            user1.PasswordHash = passwordHasher.HashPassword(user1, "user@123");

            var user2 = new User
            {
                Id = "33ee8e75-3f88-4edf-9333-a682e7a66b30",
                FullName = "Obinna Achara",
                Age = 24,
                UserName = "Obyno",
                NormalizedUserName = "OBYNO",
                Email = "obyno@gmail.com",
                NormalizedEmail = "OBYNO@GMAIL.COM",
                Address = "Agbani Road",
                State = "Enugu State",
                Country = "Nigeria",
                Active = true
            };
            user2.PasswordHash = passwordHasher.HashPassword(user2, "user@123");

            builder.HasData(
                user1,
                user2
             );
        }
    }
}
