﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WalletAPI.Entities.Configuration
{
    public class RolesConfiguration : IEntityTypeConfiguration<IdentityRole>
    {
        public void Configure(EntityTypeBuilder<IdentityRole> builder)
        {
            builder.HasData(
                 new IdentityRole
                 {
                     Id = "609b5171-ffd7-4e91-ba45-12a2773f0436",
                     Name = "Admin",
                     NormalizedName = "ADMIN"
                 },
               new IdentityRole
               {
                   Id = "67d1bcac-d66d-4626-bfd5-55dbe124fce5",
                   Name = "User",
                   NormalizedName = "USER"
               }
              
             );
        }
    }
}
