﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WalletAPI.Entities.Configuration
{
    public class UserRolesConfiguration : IEntityTypeConfiguration<IdentityUserRole<string>>
    {
        public void Configure(EntityTypeBuilder<IdentityUserRole<string>> builder)
        {
            builder.HasData(
                 new IdentityUserRole<string>
                 {
                     RoleId = "609b5171-ffd7-4e91-ba45-12a2773f0436",
                     UserId = "336ee44d-e8ac-42ab-a102-41f4cd2f13dc"
                 },
                   new IdentityUserRole<string>
                   {
                       RoleId = "67d1bcac-d66d-4626-bfd5-55dbe124fce5",
                       UserId = "33ee8e75-3f88-4edf-9333-a682e7a66b30"

                   }
            );
        }
    }
}
