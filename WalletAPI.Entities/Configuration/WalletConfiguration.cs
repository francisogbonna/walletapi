﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WalletAPI.Entities.Model;

namespace WalletAPI.Entities.Configuration
{
    public class WalletConfiguration : IEntityTypeConfiguration<Wallet>
    {
        public void Configure(EntityTypeBuilder<Wallet> builder)
        {
            builder.HasData(
                new Wallet
                {
                    Id = new Guid("aa537f47-66ed-4ea4-b14a-78831b0528d7"),
                    Balance = 15000.45M,
                    UserId = "336ee44d-e8ac-42ab-a102-41f4cd2f13dc"
                },
                new Wallet
                {
                    Id = new Guid("db64af62-7815-4295-9a4a-d4e2f74d7c8b"),
                    Balance = 3000.45M,
                    UserId = "33ee8e75-3f88-4edf-9333-a682e7a66b30"
                }
              );
        }
    }
}
