﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WalletAPI.Entities.Model
{
    public class User : IdentityUser
    {

        [Required(ErrorMessage ="Name is required")]
        public string  FullName { get; set; }

        [Required(ErrorMessage = "User must be above 18 years old.")]
        public int Age { get; set; }

        [Required(ErrorMessage ="Address field is required.")]
        public string  Address { get; set; }

        [Required(ErrorMessage = "State field is required.")]
        public string  State { get; set; }
        public string  Country { get; set; }
        public bool Active { get; set; } = true;

        public Wallet Wallet { get; set; }
        public string CreatedAt { get; set; } = DateTime.Now.ToString();
        public string UpdatedAt { get; set; } = DateTime.Now.ToString();
    }
}
