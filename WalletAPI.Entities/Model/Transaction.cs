﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WalletAPI.Entities.Model
{
    public class Transaction
    {
        [Column("TransactioinId")]
        public Guid Id { get; set; }

        [ForeignKey(nameof(Wallet))]
        public Guid WalletId { get; set; }

        public string TransactionType { get; set; }
        public string  Description { get; set; }

        [Required(ErrorMessage ="amount is required.")]
        [Column(TypeName ="decimal(18,2)")]
        public decimal Amount { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public decimal Balance { get; set; }

        public string Date { get; set; } = DateTime.Now.ToString();
    }
}
