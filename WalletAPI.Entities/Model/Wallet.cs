﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WalletAPI.Entities.Model
{
    public class Wallet
    {
        [Column("WalletId")]
        public Guid Id { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public decimal  Balance { get; set; }

        [ForeignKey(nameof(User))]
        public string UserId { get; set; }
        public User User { get; set; }

    }
}
