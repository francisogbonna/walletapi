﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WalletAPI.Contract.Repository;
using WalletAPI.Contract.Services;
using WalletAPI.Entities.Model;

namespace WalletAPI.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly IRepositoryManager _repository;
        private readonly IRepositoryBase<Transaction> _transactions;

        public TransactionService(IRepositoryManager repo)
        {
            _repository = repo;
            _transactions = _repository.GetRepository<Transaction>();
        }

        public void CreateTransaction(Transaction transaction)
        {
            _transactions.Create(transaction);
        }

        public async Task<IEnumerable<Transaction>> GetTransaction(Guid walletId, bool trackChanges)
        {
            return (await _transactions.FindByCondition(t => t.WalletId.Equals(walletId), trackChanges).ToListAsync());
        }
    }
}
