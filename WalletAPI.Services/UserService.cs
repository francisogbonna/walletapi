﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WalletAPI.Contract.Repository;
using WalletAPI.Contract.Services;
using WalletAPI.Entities.DataTransferObject.UserDTO;
using WalletAPI.Entities.Model;

namespace WalletAPI.Services
{
    public class UserService : IUserServices
    {
        private readonly IRepositoryBase<User> _user;
        private readonly IRepositoryManager _repository;
        private readonly UserManager<User> _userManager;

        public UserService(IRepositoryManager repo, UserManager<User> manager)
        {
            _repository = repo;
            _user = _repository.GetRepository<User>();
            _userManager = manager;
        }

        public async Task<User> Login(LoginDTO credentials, bool trackChanges) => await _user.FindByCondition(u =>
            ( u.Email.Equals(credentials.Email) && u.Active.Equals(true)), trackChanges).FirstOrDefaultAsync();
        public async Task<User> GetUser(string userId, bool trackChanges) => await _user.FindByCondition(u => 
            (u.Id.Equals(userId) && u.Active.Equals(true)), trackChanges).FirstOrDefaultAsync();
        public IEnumerable<User> GetAllUsers(bool trackChanges) => _user.FindAll(trackChanges).OrderBy(c => c.FullName).ToList();

        public void DeactivateAccount(string userId, bool trackChanges)
        {
            var user = _user.FindByCondition(u =>
            (u.Id.Equals(userId) && u.Active.Equals(true)), trackChanges:true).FirstOrDefault();
            user.Active = false;
            _repository.SaveChanges();
        } 
        public void ActivateAccount(string userId, bool trackChanges)
        {
            var user = _user.FindByCondition(u =>
            (u.Id.Equals(userId) && u.Active.Equals(false)), trackChanges:true).FirstOrDefault();
            user.Active = true;
            _repository.SaveChanges();
        }
    }
}
