﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WalletAPI.Contract.Repository;
using WalletAPI.Contract.Services;
using WalletAPI.Entities.DataTransferObject.TransactionDTO;
using WalletAPI.Entities.DataTransferObject.WalletDTO;
using WalletAPI.Entities.Model;

namespace WalletAPI.Services
{
    public class WalletService : IWalletServices
    {
        private readonly IRepositoryManager _repository;
        private readonly IRepositoryBase<Wallet> _wallet;
        private readonly ITransactionService _transaction;
        private readonly IUserServices _userSevice;

        public WalletService(IRepositoryManager repo, IUserServices service, ITransactionService transaction)
        {
            _repository = repo;
            _wallet = _repository.GetRepository<Wallet>();
            _transaction = transaction;
            _userSevice = service;
        }

        public async Task FundWallet(WalletTransactionDTO details)
        {
            var desc = new Transaction
            {
                WalletId = details.WalletId,
                TransactionType = "Credit",
                Description = "Personal Wallet Funding",
                Amount = details.Amount
            };
            var wallet = await _wallet.FindByCondition(c => c.Id.Equals(details.WalletId), trackChanges:true).FirstOrDefaultAsync();
            wallet.Balance += details.Amount;
            desc.Balance = wallet.Balance;

            _transaction.CreateTransaction(desc);
            await _repository.SaveChangesAsync();
        }

        public async Task<string> TransferFund(Guid sender,WalletTransactionDTO details)
        {
            var desc = new Transaction
            {
                WalletId = sender,
                TransactionType = "Debit",
                Description = $"Fund Transfer to {details.WalletId}",
                Amount = details.Amount
            };
            var wallet = await _wallet.FindByCondition(c => c.Id.Equals(sender), trackChanges: true).FirstOrDefaultAsync();
            var receivingWallet = await _wallet.FindByCondition(c => c.Id.Equals(details.WalletId), trackChanges: true).FirstOrDefaultAsync();
            if(wallet.Balance < details.Amount)
            {
                return $"Insufficient Fund! Transaction aborted";
            }

            if(receivingWallet == null)
            {
                return "Error";
            }
            wallet.Balance -= details.Amount;
            receivingWallet.Balance += details.Amount;
            desc.Balance = wallet.Balance;

            _transaction.CreateTransaction(desc);
            await _repository.SaveChangesAsync();

            return $"Fund Transfered successfully.";
        }       
        public async Task<string> PayBills(Guid sender,WalletTransactionDTO details)
        {
            var desc = new Transaction
            {
                WalletId = sender,
                TransactionType = "Debit",
                Description = $"Bill Payment to {details.WalletId}",
                Amount = details.Amount
            };
            var wallet = await _wallet.FindByCondition(c => c.Id.Equals(sender), trackChanges: true).FirstOrDefaultAsync();
            var receivingWallet = await _wallet.FindByCondition(c => c.Id.Equals(details.WalletId), trackChanges: true).FirstOrDefaultAsync();
            if(wallet.Balance < details.Amount)
            {
                return $"Insufficient Fund! Transaction aborted";
            }
            if (receivingWallet == null)
            {
                return "Error";
            }
            wallet.Balance -= details.Amount;
            receivingWallet.Balance += details.Amount;
            desc.Balance = wallet.Balance;

            _transaction.CreateTransaction(desc);
            await _repository.SaveChangesAsync();

            return $"Fund Transfered successfully.";
        }

        public async  Task<Wallet> GetWallet(string userId, bool trackChanges)
        {
            return (await _wallet.FindByCondition(c => c.UserId.Equals(userId), trackChanges).FirstOrDefaultAsync());
        }
        public async  Task<IEnumerable<Wallet>> GetAllWallets(bool trackChanges)
        {
            return (await _wallet.FindAll(trackChanges).ToListAsync());
        }
        
        

    }
}
