﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WalletAPI.Contract;
using WalletAPI.Contract.Services;
using WalletAPI.Entities.DataTransferObject.TransactionDTO;
using WalletAPI.Entities.DataTransferObject.UserDTO;
using WalletAPI.Entities.DataTransferObject.WalletDTO;
using WalletAPI.Entities.Model;
using WalletAPI.WalletApp.ActionFilters;

namespace WalletAPI.WalletApp.Controllers
{
    [Route("api/users")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserServices _userService;
        private readonly IWalletServices _wallet;
        private readonly ITransactionService _transaction;
        private readonly ILoggerManager _logger;
        private readonly IMapper _mapper;
        private readonly IAuthenticationManager _authentication;
        private readonly UserManager<User> _userManager;

        public UsersController(IUserServices service, ILoggerManager log, IMapper mapper, UserManager<User> manager,
            IAuthenticationManager auth, IWalletServices wallet, ITransactionService transaction)
        {
            _userService = service;
            _logger = log;
            _mapper = mapper;
            _userManager = manager;
            _authentication = auth;
            _wallet = wallet;
            _transaction = transaction;
        }

        [HttpGet, Authorize(Roles ="Admin")]
        public IActionResult GetAllUsers()
        {
            var users = _userService.GetAllUsers(trackChanges:false);
            if (users == null)
            {
                _logger.LogInfo($"No Active user  was found in the database.");
                return NotFound();
            }
            var usersFound = _mapper.Map<IEnumerable<UserViewDTO>>(users);
            return Ok(usersFound);
        }

        [HttpGet("wallets")]
        public async Task<IActionResult> GetAllWallets()
        {
            var wallets =await _wallet.GetAllWallets(trackChanges: false);
            var availableWallets = _mapper.Map<IEnumerable<WalletViewDTO>>(wallets);
            return Ok(availableWallets);
        }

        [HttpGet("{userId}", Name ="UserById")]
        [ServiceFilter(typeof(UserExistActionFilter))]
        public IActionResult GetSingleUser(string userId)
        {
            var user = HttpContext.Items["user"] as User;
            var foundUser = _mapper.Map<UserViewDTO>(user);
            return Ok(foundUser);
        }
        
        [HttpPost("login")]
        [ServiceFilter(typeof(ValidationFilter))]
        public async Task<IActionResult> Login([FromBody]LoginDTO credentials)
        {
            if (! await _authentication.ValidateUser(credentials))
            {
                _logger.LogWarn($"{nameof(Login)}: Invalid Email and Password.");
                return Unauthorized("Invalid Email and Password.");
            }

            return Ok(new {Token = await _authentication.CreateToken() });
        }

        [HttpPost("register")]
        [ServiceFilter(typeof(ValidationFilter))]
        public async Task< IActionResult> Register([FromBody]CreateUserDTO user)
        {
            var newUser = _mapper.Map<User>(user);
            var result = await _userManager.CreateAsync(newUser, user.Password);
            if(!result.Succeeded)
            {
                foreach (var error  in result.Errors)
                {
                    ModelState.TryAddModelError(error.Code, error.Description);
                }
                _logger.LogError($"Error: {ModelState}");
                return BadRequest(ModelState);
            }
            await _userManager.AddToRoleAsync(newUser, "User");

            return Ok(new { message = "Registration Successful"});
        }

        [HttpGet("{userId}/wallet"), Authorize(Roles = "User")]
        [ServiceFilter(typeof(UserExistActionFilter))]
        public async Task<IActionResult> GetWallet(string userId)
        {
            var user = HttpContext.Items["user"] as User;
            var wallet = await _wallet.GetWallet(userId, trackChanges: false);
            var walletToReturn = _mapper.Map<WalletViewDTO>(wallet);
            return Ok(walletToReturn);
        }

        [HttpDelete("delete-account/{userId}"), Authorize(Roles = "User")]
        public IActionResult  DeleteAccount(string userId)
        {
            _userService.DeactivateAccount(userId, trackChanges: false);
            return NoContent();
        }  
        
        [HttpPatch("Activate-account/{userId}"), Authorize(Roles = "Admin")]
        public IActionResult  ActivateAccount(string userId)
        {
            _userService.ActivateAccount(userId, trackChanges: false);
            return NoContent();
        }

        [HttpPut("{userId}/wallet/deposit"), Authorize(Roles = "User")]
        [ServiceFilter(typeof(ValidationFilter))]
        public async Task<IActionResult> FundWallet([FromBody]WalletTransactionDTO walletTransactionDTO)
        {
            await _wallet.FundWallet(walletTransactionDTO);
            return Ok(new { message = $"{walletTransactionDTO.Amount} deposited successfully." });
        }

        [HttpGet("{userId}/wallet/transactions/{walletId}"), Authorize(Roles = "User")]
        public async Task<IActionResult> ViewTransactions(Guid walletId)
        {
            var transaction =await _transaction.GetTransaction(walletId, trackChanges: false);
            if(transaction.Count() == 0)
            {
                _logger.LogInfo($"No Transaction activity for wallet ID: {walletId}");
                return NoContent();
            }
            var userTransactions = _mapper.Map<IEnumerable<TransactionViewDTO>>(transaction);
            return Ok(userTransactions);
        }

        [HttpPut("{userId}/wallet/{walletId}/transfer"), Authorize(Roles = "User")]
        [ServiceFilter(typeof(ValidationFilter))]
        public async Task<IActionResult> TransferFund(Guid walletId,[FromBody]WalletTransactionDTO walletTransactionDTO)
        {
           
            if (walletTransactionDTO.WalletId.Equals(walletId))
            {
                _logger.LogError($"Invalid input");
                return BadRequest(new { Error = "You can't transfer to same wallet address" });
            }

            var message = await _wallet.TransferFund(walletId, walletTransactionDTO);
            if (message.Equals("Error")) return BadRequest($"Reciever's Wallet Address is invalid");

            return Ok(message);
        } 
        
        [HttpPut("{userId}/wallet/{walletId}/utility-payment"), Authorize(Roles = "User")]
        [ServiceFilter(typeof(ValidationFilter))]
        public async Task<IActionResult> PayBills(Guid walletId,[FromBody]WalletTransactionDTO walletTransactionDTO)
        {
           
            if (walletTransactionDTO.WalletId.Equals(walletId))
            {
                _logger.LogError($"Invalid input");
                return BadRequest(new { Error = "You can't transfer to same wallet address" });
            }

            var message = await _wallet.PayBills(walletId, walletTransactionDTO);
            if (message.Equals("Error")) return BadRequest($"Reciever's Wallet Address is invalid");
            return Ok(message);
        }

    }
}
