﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WalletAPI.Contract;
using WalletAPI.Contract.Repository;
using WalletAPI.Contract.Services;
using WalletAPI.Entities.Model;
using WalletAPI.LoggerServices;
using WalletAPI.Repository;
using WalletAPI.Services;
using WalletAPI.WalletApp.ActionFilters;

namespace WalletAPI.WalletApp.Extensions
{
    public static class ServiceExtensions
    {
        public static void ConfigureCors(this IServiceCollection services) => services.AddCors(options =>
        {
            options.AddPolicy("CorsPolicy", builder => builder.AllowAnyOrigin()
            .AllowAnyMethod()
            .AllowAnyHeader());
        });
        public static void ConfigureIISIntegration(this IServiceCollection services) => services.Configure<IISOptions>(options =>
        {

        });
        public static void ConfigureLoggerService(this IServiceCollection services) => services.AddScoped<ILoggerManager, LoggerManager>();
        public static void ConfigureSqlContext(this IServiceCollection services, IConfiguration configuration) =>
            services.AddDbContext<WalletDBContext>(opts => opts.UseSqlServer(configuration.GetConnectionString("sqlConnection"), b =>
            b.MigrationsAssembly("WalletAPI.WalletApp")));

        public static void ConfigureIdentity(this IServiceCollection services)
        {
            var builder = services.AddIdentityCore<User>(user =>
            {
                user.Password.RequireDigit = true;
                user.Password.RequiredLength = 8;
                user.Password.RequireLowercase = false;
                user.Password.RequireNonAlphanumeric = false;
                user.Password.RequireUppercase = false;
                user.User.RequireUniqueEmail = true;
            });
            builder = new IdentityBuilder(builder.UserType, typeof(IdentityRole), builder.Services);
            builder.AddEntityFrameworkStores<WalletDBContext>().AddDefaultTokenProviders();
        }

        public static void ConfigureJWT(this IServiceCollection services, IConfiguration configuration)
        {
            var jwtSettings = configuration.GetSection("JwtSettings");
            var secretKey = jwtSettings.GetSection("secret").Value;

            services.AddAuthentication(opt =>
            {
                opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
             .AddJwtBearer(option =>
             {
                 option.TokenValidationParameters = new TokenValidationParameters
                 {
                     ValidateIssuer = true,
                     ValidateAudience = true,
                     ValidateLifetime = true,
                     ValidateIssuerSigningKey = true,

                     ValidIssuer = jwtSettings.GetSection("validIssuer").Value,
                     ValidAudience = jwtSettings.GetSection("validAudience").Value,
                     IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey))
                 };
             });
        }

        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddScoped<DbContext, WalletDBContext>();
            services.AddScoped<IRepositoryManager, RepositoryManager<WalletDBContext>>();
            services.AddScoped<IUserServices, UserService>();
            services.AddScoped<IAuthenticationManager, AuthenticationManager>();
            services.AddScoped<IWalletServices, WalletService>();
            services.AddScoped<ITransactionService, TransactionService>();
            services.AddScoped<ValidationFilter>();
            services.AddScoped<UserExistActionFilter>();
        }
    }
}
