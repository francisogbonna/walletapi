﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WalletAPI.Entities.DataTransferObject.TransactionDTO;
using WalletAPI.Entities.DataTransferObject.UserDTO;
using WalletAPI.Entities.DataTransferObject.WalletDTO;
using WalletAPI.Entities.Model;

namespace WalletAPI.WalletApp.Extensions
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<CreateUserDTO, User>();
            CreateMap<CreateWalletDTO, Wallet>()
                .ForMember(c => c.Balance, src => src.MapFrom(b => b.Opening_Balance)).ReverseMap();
            CreateMap<CreateTransactionDTO, Transaction>();

            //views
            CreateMap<User, UserViewDTO>()
                .ForMember(a => a.FullAddress, src => src.MapFrom(c => $"{c.Address}, {c.State} {c.Country}"));
            CreateMap<Wallet, WalletViewDTO>()
                .ForMember(w => w.WalletId, src => src.MapFrom(c => c.Id));
            CreateMap<Transaction, TransactionViewDTO>();

            //authentication
            CreateMap<User, LoginDTO>();
        }
    }
}
