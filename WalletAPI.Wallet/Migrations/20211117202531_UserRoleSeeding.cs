﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WalletAPI.WalletApp.Migrations
{
    public partial class UserRoleSeeding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "609b5171-ffd7-4e91-ba45-12a2773f0436",
                column: "ConcurrencyStamp",
                value: "8b0b9fe1-b6c3-4d4f-948d-c9999aefc3c5");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "67d1bcac-d66d-4626-bfd5-55dbe124fce5",
                column: "ConcurrencyStamp",
                value: "31c40685-edf6-47ce-8de9-f71d115fc5c6");

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[,]
                {
                    { "609b5171-ffd7-4e91-ba45-12a2773f0436", "336ee44d-e8ac-42ab-a102-41f4cd2f13dc" },
                    { "67d1bcac-d66d-4626-bfd5-55dbe124fce5", "33ee8e75-3f88-4edf-9333-a682e7a66b30" }
                });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "336ee44d-e8ac-42ab-a102-41f4cd2f13dc",
                columns: new[] { "ConcurrencyStamp", "CreatedAt", "PasswordHash", "SecurityStamp", "UpdatedAt" },
                values: new object[] { "d92418b3-6716-4809-871c-e354a13baf2d", "17/11/2021 9:25:28 PM", "AQAAAAEAACcQAAAAELj5UVXcXvnnTwERCk+3txpskMuPVU0Y7/NFYiEQDNxBvi54FoQtT/ciubZ4NcZ8Kw==", "d4f20a75-4d6c-433a-a816-743c30e4cbcc", "17/11/2021 9:25:28 PM" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "33ee8e75-3f88-4edf-9333-a682e7a66b30",
                columns: new[] { "ConcurrencyStamp", "CreatedAt", "PasswordHash", "SecurityStamp", "UpdatedAt" },
                values: new object[] { "ec4867b1-c6c0-43b7-b96e-9f812e5abccd", "17/11/2021 9:25:28 PM", "AQAAAAEAACcQAAAAEE4xcHF2IfHmmBb5eMOEHFRR45k1rmuUX3LAk2HW8M15eXlqzpGtkdzWXzFLLkXVqQ==", "892f138a-3c6e-4644-835b-e14394a5d04e", "17/11/2021 9:25:28 PM" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "609b5171-ffd7-4e91-ba45-12a2773f0436", "336ee44d-e8ac-42ab-a102-41f4cd2f13dc" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "67d1bcac-d66d-4626-bfd5-55dbe124fce5", "33ee8e75-3f88-4edf-9333-a682e7a66b30" });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "609b5171-ffd7-4e91-ba45-12a2773f0436",
                column: "ConcurrencyStamp",
                value: "10dccc7f-9667-4806-9523-cc2f0e9fc6ee");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "67d1bcac-d66d-4626-bfd5-55dbe124fce5",
                column: "ConcurrencyStamp",
                value: "2d13a3be-5d2f-4d6b-a777-7e1ac38732d3");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "336ee44d-e8ac-42ab-a102-41f4cd2f13dc",
                columns: new[] { "ConcurrencyStamp", "CreatedAt", "PasswordHash", "SecurityStamp", "UpdatedAt" },
                values: new object[] { "74d10c24-f370-4a44-9451-d3c65388d272", "17/11/2021 9:24:19 PM", "AQAAAAEAACcQAAAAEHjYAoLk+s5Yh+gdBJFrNw5ZHR0jM6fN8OTKX1X6vQ585ujEJ4gF+vA5fm+XjvTqzw==", "ee0ef567-3312-4254-a924-6c4803292607", "17/11/2021 9:24:19 PM" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "33ee8e75-3f88-4edf-9333-a682e7a66b30",
                columns: new[] { "ConcurrencyStamp", "CreatedAt", "PasswordHash", "SecurityStamp", "UpdatedAt" },
                values: new object[] { "f2740292-a3bc-41be-902c-5e5ccec795ff", "17/11/2021 9:24:19 PM", "AQAAAAEAACcQAAAAELTMB4JJVF9fPYhN6f1l3uMP8CLA50MGP2tfn/JINgcTGDmmKnxQknhw93Ft8lo8Og==", "5bbcc75d-1917-4c48-a7b7-d14442047a12", "17/11/2021 9:24:19 PM" });
        }
    }
}
