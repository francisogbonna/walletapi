﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WalletAPI.WalletApp.Migrations
{
    public partial class UpdatedUserEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Password",
                table: "AspNetUsers");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "609b5171-ffd7-4e91-ba45-12a2773f0436",
                column: "ConcurrencyStamp",
                value: "98adc5c7-1a8b-4f88-b0bf-b29b2b165dc7");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "67d1bcac-d66d-4626-bfd5-55dbe124fce5",
                column: "ConcurrencyStamp",
                value: "4c41f0dd-2af0-4675-b507-3bce2caab8a2");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "336ee44d-e8ac-42ab-a102-41f4cd2f13dc",
                columns: new[] { "ConcurrencyStamp", "CreatedAt", "PasswordHash", "SecurityStamp", "UpdatedAt" },
                values: new object[] { "90382517-3e24-4500-bd28-609cbfc041f7", "17/11/2021 9:56:26 PM", "AQAAAAEAACcQAAAAEOmgR7yz8hmKkWlZcIGLUnV0RXpwUVc1yFrve6q29CbVZwWdGiH1utMlqolespdD3g==", "6d7b94f2-96b6-4e8b-a196-a71c0f263e1c", "17/11/2021 9:56:26 PM" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "33ee8e75-3f88-4edf-9333-a682e7a66b30",
                columns: new[] { "ConcurrencyStamp", "CreatedAt", "PasswordHash", "SecurityStamp", "UpdatedAt" },
                values: new object[] { "7c7a8834-bade-4e54-80bf-a89eb5a36108", "17/11/2021 9:56:26 PM", "AQAAAAEAACcQAAAAEFYjJMWgnojTBMHLiMM8CVagtwWb4/iEkEC1bYfFc0ndTO12o1h8hqpkcaJ33my5MA==", "8fcf09bf-cb8d-497b-a703-4057b74519c4", "17/11/2021 9:56:26 PM" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Password",
                table: "AspNetUsers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "609b5171-ffd7-4e91-ba45-12a2773f0436",
                column: "ConcurrencyStamp",
                value: "8b0b9fe1-b6c3-4d4f-948d-c9999aefc3c5");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "67d1bcac-d66d-4626-bfd5-55dbe124fce5",
                column: "ConcurrencyStamp",
                value: "31c40685-edf6-47ce-8de9-f71d115fc5c6");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "336ee44d-e8ac-42ab-a102-41f4cd2f13dc",
                columns: new[] { "ConcurrencyStamp", "CreatedAt", "PasswordHash", "SecurityStamp", "UpdatedAt" },
                values: new object[] { "d92418b3-6716-4809-871c-e354a13baf2d", "17/11/2021 9:25:28 PM", "AQAAAAEAACcQAAAAELj5UVXcXvnnTwERCk+3txpskMuPVU0Y7/NFYiEQDNxBvi54FoQtT/ciubZ4NcZ8Kw==", "d4f20a75-4d6c-433a-a816-743c30e4cbcc", "17/11/2021 9:25:28 PM" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "33ee8e75-3f88-4edf-9333-a682e7a66b30",
                columns: new[] { "ConcurrencyStamp", "CreatedAt", "PasswordHash", "SecurityStamp", "UpdatedAt" },
                values: new object[] { "ec4867b1-c6c0-43b7-b96e-9f812e5abccd", "17/11/2021 9:25:28 PM", "AQAAAAEAACcQAAAAEE4xcHF2IfHmmBb5eMOEHFRR45k1rmuUX3LAk2HW8M15eXlqzpGtkdzWXzFLLkXVqQ==", "892f138a-3c6e-4644-835b-e14394a5d04e", "17/11/2021 9:25:28 PM" });
        }
    }
}
