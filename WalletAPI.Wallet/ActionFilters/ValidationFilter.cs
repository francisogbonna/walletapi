﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WalletAPI.Contract;

namespace WalletAPI.WalletApp.ActionFilters
{
    public class ValidationFilter  : IActionFilter
    {
        private readonly ILoggerManager _logger;

        public ValidationFilter(ILoggerManager log)
        {
            _logger = log;
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var action = context.RouteData.Values["action"];
            var controller = context.RouteData.Values["controller"];
            var parameter = context.ActionArguments.SingleOrDefault(c => c.ToString().Contains("DTO")).Value;
            if(parameter == null)
            {
                _logger.LogError($"Data sent from client is null. Controller: {controller} Action: {action}");
                context.Result = new BadRequestObjectResult($"Data sent from client is null. Controller: {controller} Action: {action}");
                return;
            }
            if (!context.ModelState.IsValid)
            {
                _logger.LogError($"Invalid ModelState for object. Controller: {controller} Action: {action}");
                context.Result = new UnprocessableEntityObjectResult(context.ModelState);
            }
        }

        public void OnActionExecuted(ActionExecutedContext context) {}

        
    }
}
