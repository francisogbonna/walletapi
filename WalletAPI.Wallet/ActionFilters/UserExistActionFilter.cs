﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WalletAPI.Contract;
using WalletAPI.Contract.Repository;
using WalletAPI.Entities.Model;

namespace WalletAPI.WalletApp.ActionFilters
{
    public class UserExistActionFilter : IAsyncActionFilter
    {
        private readonly IRepositoryManager _repository;
        private readonly IRepositoryBase<User> _user;
        private readonly ILoggerManager _logger;

        public UserExistActionFilter(IRepositoryManager repo, ILoggerManager log)
        {
            _logger = log;
            _repository = repo;
            _user = _repository.GetRepository<User>();
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var trackChanges = context.HttpContext.Request.Method.Equals("PUT");
            var id = context.ActionArguments["userId"];
            var user = await _user.FindByCondition(x => (x.Id.Equals(id) && x.Active.Equals(true)), trackChanges).FirstOrDefaultAsync();
            if(user == null)
            {
                _logger.LogError($"User with ID {id} not found.");
                context.Result = new NotFoundResult();
            }
            else
            {
                context.HttpContext.Items.Add("user", user);
                await next();
            }
        }
    }
}
