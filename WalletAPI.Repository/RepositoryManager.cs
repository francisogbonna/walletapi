﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WalletAPI.Contract.Repository;
using WalletAPI.Entities.Model;

namespace WalletAPI.Repository
{
    public class RepositoryManager<TContext> : IRepositoryManager<DbContext> where TContext : DbContext
    {
        private Dictionary<Type, object> _repositories;
        private readonly WalletDBContext _context;

        public RepositoryManager(WalletDBContext context)
        {
            _context = context ?? throw new ArgumentException(nameof(_context));
        }
        public IRepositoryBase<T> GetRepository<T>() where T : class
        {
            if(_repositories is null) _repositories = new Dictionary<Type, object>();
            var type = typeof(T);
            if (!_repositories.ContainsKey(type)) _repositories[type] = new RepositoryBase<T>(_context);
            return (IRepositoryBase<T>)_repositories[type];
        }

        public void SaveChanges() => _context.SaveChanges();

        public async Task SaveChangesAsync() => await _context.SaveChangesAsync();
    }
}
