﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WalletAPI.Entities.DataTransferObject.UserDTO;

namespace WalletAPI.Contract
{
    public interface IAuthenticationManager
    {
        Task<bool> ValidateUser(LoginDTO loginDetails);
        Task<string> CreateToken();
    }
}
