﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WalletAPI.Contract.Repository
{
    public interface IRepositoryManager
    {
        IRepositoryBase<T> GetRepository<T>() where T : class;
        Task SaveChangesAsync();
        void SaveChanges();
    }
    public interface IRepositoryManager<TContext> : IRepositoryManager
    {

    }
}
