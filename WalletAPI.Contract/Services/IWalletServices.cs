﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WalletAPI.Entities.DataTransferObject.WalletDTO;
using WalletAPI.Entities.Model;

namespace WalletAPI.Contract.Services
{
    public interface IWalletServices
    {
        Task<Wallet> GetWallet(string userId, bool trackChanges);
        Task<IEnumerable<Wallet>> GetAllWallets(bool trackChanges);
        Task FundWallet(WalletTransactionDTO details);
        Task<string> TransferFund(Guid sender,WalletTransactionDTO details);
        Task<string> PayBills(Guid sender,WalletTransactionDTO details);
    }
}
