﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WalletAPI.Entities.Model;

namespace WalletAPI.Contract.Services
{
    public interface ITransactionService
    {
        void CreateTransaction(Transaction transaction);
        Task<IEnumerable<Transaction>> GetTransaction(Guid walletId, bool trackChanges);
    }
}
