﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WalletAPI.Entities.DataTransferObject.UserDTO;
using WalletAPI.Entities.Model;

namespace WalletAPI.Contract.Services
{
    public interface IUserServices
    {
        Task <User> GetUser(string userId, bool trackChanges);
        IEnumerable<User> GetAllUsers(bool trackChanges);
        void DeactivateAccount(string userId, bool trackChanges);
        void ActivateAccount(string userId, bool trackChanges);
    }
}
